package com.investree.demo.model;

import com.fasterxml.jackson.databind.PropertyNamingStrategies;
import com.fasterxml.jackson.databind.annotation.JsonNaming;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.List;

@NoArgsConstructor
@AllArgsConstructor
@Data
@Builder
@Entity
@Table(name = "t_transaksi")
@JsonNaming(PropertyNamingStrategies.SnakeCaseStrategy.class)
public class Transaksi {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @ManyToOne
    @JoinColumn(name = "id_peminjam", nullable = false)
    private User peminjam;

    @ManyToOne
    @JoinColumn(name = "id_meminjam", nullable = false)
    private User meminjam;

    @Column(nullable = false)
    private Integer tenor;

    @Column(name = "total_pinjaman", nullable = false)
    private Double totalPinjaman;

    @Column(name = "bunga_persen", nullable = false)
    private Double bungaPersen;

    @Column(name = "status")
    private String status;

    @OneToMany(mappedBy = "transaksi")
    private List<PaymentHistory> paymentHistories;

}
