package com.investree.demo.model;

import com.fasterxml.jackson.databind.PropertyNamingStrategies;
import com.fasterxml.jackson.databind.annotation.JsonNaming;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@NoArgsConstructor
@AllArgsConstructor
@Data
@Builder
@Entity
@Table(name = "t_historypembayaran")
@JsonNaming(PropertyNamingStrategies.SnakeCaseStrategy.class)
public class PaymentHistory {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @ManyToOne
    @JoinColumn(name = "id_transaksi", nullable = false)
    private Transaksi transaksi;

    @Column(name = "pembayaran_ke", nullable = false)
    private Integer pembayaranKe;

    @Column(nullable = false)
    private Double jumlah;

    @Column(name = "bukti_pembayaran", nullable = false)
    private String buktiPembayaran;

}
